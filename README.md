# Freedesktop-sdk cache and releases server

## Setup

There are a few secrets that need to be installed manually:
* copy `flat-manager.json.in` to `flat-manager.json` and set the secret in it;
* copy the GnuPG directory containing the keys used for signing flatpak commits to `gnupg`.

To start, run `docker-compose up -d`.

## Rotating CAS Push Secrets

Generate a new certificate and key via:

```
openssl req -new -newkey rsa:4096 -x509 -sha256 -days $((365 * 5)) -nodes \
  -out authorized.crt -keyout certificate.key
```

Leave any fields empty when prompted by entering a `.`, except for the Common
Name (CN) which is set to `freedesktop-sdk-artifacts`.

Commit the updated certificate (not the key, though `*.key` files will be
ignored by git) and create an MR.

To redeploy `traefik` with the new certificate run:

```
docker-compose up -d traefik
```

Finally update the group-level
[CI/CD variables](https://gitlab.com/groups/freedesktop-sdk/-/settings/ci_cd)
with the content of the new secret files.

The generated `certificate.key` can now be deleted from the local filesystem.
